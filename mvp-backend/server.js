const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');
const app = express();
const PORT = 3000;
const path = require('path');

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.resolve('./public')));

const readData = () => {
  const data = fs.readFileSync('data.json');
  return JSON.parse(data);
};

const writeData = (data) => {
  fs.writeFileSync('data.json', JSON.stringify(data, null, 2));
};

const multer = require('multer');
const upload = multer({ dest: 'public/uploads/' });

app.post('/products', upload.single('image'), (req, res) => {
  const { name, price, quantity } = req.body;
  const imageUrl = req.file ? req.file.filename : null; // Stocker uniquement le nom du fichier avec son extension

  const data = readData();
  const newProduct = {
    id: data.products.length + 1,
    name,
    price,
    quantity,
    imageUrl,
  };
  data.products.push(newProduct);
  writeData(data);

  res.status(201).json({ message: 'Produit ajouté avec succès', product: newProduct });
});

app.get('/products', (req, res) => {
  const data = readData();
  res.json(data.products);
});

app.post('/cart', (req, res) => {
  const { productId, quantity } = req.body;
  const data = readData();
  const product = data.products.find(p => p.id === productId);

  if (!product || product.quantity < quantity) {
    return res.status(400).json({ error: 'Produit non disponible ou stock insuffisant' });
  }

  product.quantity -= quantity;

  let cart = data.carts.find(c => c.id === req.body.cartId);
  if (!cart) {
    cart = { id: data.carts.length + 1, products: [], created_at: new Date() };
    data.carts.push(cart);
  }

  const cartProduct = cart.products.find(p => p.id === productId);
  if (cartProduct) {
    cartProduct.quantity += quantity;
  } else {
    cart.products.push({ id: productId, quantity });
  }

  writeData(data);

  console.log('Cart updated:', cart); // Log for debugging
  res.status(200).json(cart);
});

app.get('/cart/:cartId', (req, res) => {
  const data = readData();
  const cart = data.carts.find(c => c.id === parseInt(req.params.cartId));
  if (!cart) {
    return res.status(404).json({ error: 'Panier non trouvé' });
  }
  console.log('Cart retrieved:', cart); // Log for debugging
  res.json(cart);
});

app.put('/cart/:cartId', (req, res) => {
  const { productId, quantity } = req.body;
  const data = readData();
  const cart = data.carts.find(c => c.id === parseInt(req.params.cartId));

  if (!cart) {
    return res.status(404).json({ error: 'Panier non trouvé' });
  }

  const product = data.products.find(p => p.id === productId);
  const cartProduct = cart.products.find(p => p.id === productId);

  if (!product || (quantity > 0 && product.quantity < quantity - cartProduct.quantity)) {
    return res.status(400).json({ error: 'Produit non disponible ou stock insuffisant' });
  }

  product.quantity += cartProduct.quantity - quantity;

  if (quantity > 0) {
    cartProduct.quantity = quantity;
  } else {
    cart.products = cart.products.filter(p => p.id !== productId);
  }

  writeData(data);
  console.log('Cart updated:', cart); // Log for debugging
  res.status(200).json(cart);
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
