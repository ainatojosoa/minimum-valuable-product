# Projet Vue 3 et Node.js

Ce projet utilise Vue 3 pour le frontend et Node.js pour le backend.

## Structure du Projet

### Frontend

La structure du frontend suit un modèle standard et est organisée comme suit :
- **src**
  - **pages** : Contient les différentes pages de l'application.
  - **components** : Contient les composants réutilisables dans différentes pages.
  - **router** : Gère la redirection des routes.
  - **store** : Gère le stockage de l'état global et la gestion des API, ainsi que les stockages temporaires des panier

### Backend

Le backend est construit avec Node.js et utilise un fichier JSON pour le stockage des données.

## Technologies Utilisées

- **Frontend** : Vue 3, Axios pour les appels API, Bootstrap pour la base de la mise en page et le design.
- **Backend** : Node.js.

## Installation et Lancement du Projet

### Prérequis

- Node.js doit être installé sur votre machine. Vous pouvez le télécharger et l'installer depuis [nodejs.org](https://nodejs.org/).

### Backend

1. Cloner le projet et naviguer dans le dossier `mvp-backend`.
2. Installer les dépendances avec `npm install`.
3. Lancer le serveur backend avec `node server.js`.

### Frontend

1. Cloner le projet et naviguer dans le dossier `mvp`.
2. Installer les dépendances avec `npm install`.
3. Lancer le serveur frontend avec `npm run serve`.

D'accord, voici la partie mise à jour du README avec les fonctionnalités supplémentaires :

---

## Fonctionnalités

### Utilisateur

- Affichage des produits : Les utilisateurs peuvent visualiser tous les produits disponibles.
- Ajout au panier : Les utilisateurs peuvent ajouter des produits à leur panier.
- Gestion du panier : Les utilisateurs peuvent visualiser et gérer leur liste de produits dans le panier.
- Total du prix : Les utilisateurs peuvent voir le total du prix de chaque produit en fonction de la quantité sélectionnée.
- Retrait du panier : Les utilisateurs peuvent retirer des produits de leur panier.
- Gestion des stocks : Si un produit est épuisé, le bouton correspondant sera désactivé ou changera en "Stock épuisé".

### Administrateur

- Ajout de produits : Les administrateurs peuvent ajouter de nouveaux produits, y compris l'image du produit.
