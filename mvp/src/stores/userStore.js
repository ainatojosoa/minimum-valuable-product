import {
  defineStore
} from 'pinia';
import axios from 'axios';

export const useStore = defineStore('main', {
  state: () => ({
    products: [],
    cart: [],
  }),
  actions: {
    async fetchProducts() {
      try {
        const response = await axios.get('http://localhost:3000/products');
        console.log('data:', response);
        this.products = response.data;
      } catch (error) {
        console.error('Error:', error);
      }
    },
    async addProduct(formData) {
      try {
        await axios.post('http://localhost:3000/products', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        });
      } catch (error) {
        console.error('Erreur lors de l\'ajout du produit:', error);
      }
    },
    addToCart(productId, quantity) {
      const product = this.products.find(p => p.id === productId);
      if (product && product.quantity >= quantity) {
        const cartProduct = this.cart.find(p => p.id === productId);
        if (cartProduct) {
          cartProduct.quantity += quantity;
        } else {
          this.cart.push({
            ...product,
            quantity
          });
        }
        product.quantity -= quantity;
      } else {
        console.error('Stock epuisé');
      }
    },
    removeFromCart(productId, quantity) {
      const cartProduct = this.cart.find(p => p.id === productId);
      const product = this.products.find(p => p.id === productId);

      if (cartProduct) {
        if (cartProduct.quantity > quantity) {
          cartProduct.quantity -= quantity;
          product.quantity += quantity;
        } else {
          this.cart = this.cart.filter(p => p.id !== productId);
          product.quantity += cartProduct.quantity;
        }
      }
    },
  },
});
