// src/main.js
import { createApp } from 'vue';
import App from './App.vue';
import router from './router'; // Importer le routeur
import { createPinia } from 'pinia';
import { BootstrapVueNext } from 'bootstrap-vue-next';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css';

const app = createApp(App);

app.use(createPinia());
app.use(BootstrapVueNext);
app.use(router); // Utiliser le routeur

app.mount('#app');
