import { createRouter, createWebHistory } from 'vue-router';
import ProductPage from '@/pages/ProductPage.vue';
import CreateProductPage from '@/pages/CreateProductPage.vue';

const routes = [
  { path: '/', component: ProductPage },
  { 
    path: '/add-product',
    component: CreateProductPage,
    meta: { requiresAdmin: true }
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Ajouter une vérification pour chaque navigation de route
router.beforeEach((to, from, next) => {
  const isAdmin = localStorage.getItem('isAdmin');
  if (to.meta.requiresAdmin && isAdmin !== 'true') {
    next('/');
  } else {
    next();
  }
});

export default router;
